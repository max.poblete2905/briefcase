export const GET_CONFIG_LOGIN = 'GET_CONFIG_LOGIN';
export const GET_LOGIN = 'GET_LOGIN';
export const GET_LOGIN_ERROR = 'GET_LOGIN_ERROR';
export const CERRAR_SESSION = 'CERRAR_SESSION';

//personal
export const GET_CONFIG_PERSONAL = 'GET_CONFIG_PERSONAL';
export const GET_CONFIG_FOTO = 'GET_CONFIG_FOTO';
export const GET_CONFIG_OPCIONES = 'GET_CONFIG_OPCIONES';

//modulos state
export const GET_MENU = 'GET_MENU';
export const GET_MENU_ADMIN = 'GET_MENU_ADMIN';