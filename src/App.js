import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './components/Home';
import PortafolioState from './context/authContext/portafolioState';
import PersonalState from './context/personalContext/personalState';
import ModuloState from './context/modulosContext/modulosState';

function App() {
  return (
    <div>
      <Router>
        <Switch>
          <PortafolioState>
            <PersonalState>
              <ModuloState>
                <Route path={`${process.env.PUBLIC_URL}/`} component={Home} />
              </ModuloState>
            </PersonalState>
          </PortafolioState>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
