import React, { useEffect, useContext } from 'react';
import Grid from '@material-ui/core/Grid';
import Foto from './Foto'
import Personal from './Personal';
import personalContext from '../../context/personalContext/personalContext';
import Progress from '../Progress';

export default function FullWidthGrid() {
  const { configPersonal, getConfigPersonal } = useContext(personalContext);

  useEffect(() => {
    getConfigPersonal();
  }, []);

  if (Object.keys(configPersonal).length === 0) {
    return <Progress />
  }

  return (
    <>
      <div style={{
        background: 'white',
        padding: 30,
      }}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={3}>
            <Foto />
          </Grid>
          <Grid item xs={12} sm={9}>
            <Personal />
          </Grid>
        </Grid>
      </div>
    </>
  );
}

