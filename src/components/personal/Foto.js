import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import personalContext from '../../context/personalContext/personalContext';

const useStyles = makeStyles({
    root: {
        maxWidth: '100%',
    },
    media: {
        height: 380,
    },
});

export default function MediaCard({ id }) {
    const classes = useStyles();
    const { configFoto } = useContext(personalContext);
    const { titulo, subTitulo, profesion, img } = configFoto;

    return (
        <Card className={classes.root}>
            <CardActionArea>
                <CardMedia
                    className={classes.media}
                    image={img}
                    title="Contemplative Reptile"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        <strong>{titulo}</strong>
                    </Typography>
                    <Typography gutterBottom variant="h5" component="h2">
                        {profesion}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {subTitulo}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
}
