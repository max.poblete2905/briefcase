import React, { useEffect, useState, useContext } from 'react';
import { alpha, makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import app from '../config/firebase';
import { collection, getDocs, getFirestore } from "firebase/firestore";
import Login from './Login';
import portafolioContext from '../context/authContext/portafolioContext';

const db = getFirestore(app);
const drawerWidth = 240;

const styles = {
    color: {
        solids: ['rgba(116, 72, 194, 1)', 'rgba(33, 192, 215, 1)', 'rgba(217, 158, 43, 1)', 'rgba(205, 58, 129, 1)', 'rgba(156, 153, 204, 1)', 'rgba(225, 78, 202, 1)'],
        alphas: ['rgba(116, 72, 194, .2)', 'rgba(33, 192, 215, .2)', 'rgba(217, 158, 43, .2)', 'rgba(205, 58, 129, .2)', 'rgba(156, 153, 204, .2)', 'rgba(225, 78, 202, .2)'],
    },
};

const useStyles = makeStyles((theme) => ({
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: alpha(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: alpha(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
    },
    appBar: {
        background: '#181f38',
        color: styles.color.solids[4],
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },

    hide: {
        display: 'none',
    },
    root: {
        width: '100%'
    }

}));

export default function PrimarySearchAppBar({ open, handleDrawerOpen }) {
    const classes = useStyles();
    const { cerrarSesion, lodged } = useContext(portafolioContext);
    const [header, setHeader] = useState({});

    useEffect(() => {
        const getMenuFirestore = async () => {
            const querySnapshot = await getDocs(collection(db, `/portafolio/home/header`));
            querySnapshot.forEach((doc) => {
                setHeader(doc.data())
            });
        }
        getMenuFirestore();
    }, []);

    const singOut = () => {
        cerrarSesion();
    }

    return (
        <div className={classes.grow}>
            {header ?
                <AppBar
                    position="fixed"
                    className={clsx(classes.appBar, {
                        [classes.appBarShift]: open,
                    })}
                >
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={handleDrawerOpen}
                            edge="start"
                            className={clsx(classes.menuButton, {
                                [classes.hide]: open,
                            })}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography className={classes.title} variant="h6" noWrap>
                            {header.titulo}
                        </Typography>

                        <div className={classes.grow} />
                        {lodged ?
                            <div className={classes.sectionDesktop}>
                                <IconButton
                                    edge="end"
                                    aria-haspopup="true"
                                    color="inherit"
                                    onClick={() => singOut()}
                                >
                                    logout
                                </IconButton>
                            </div>
                            :
                            <div className={classes.sectionMobile}>
                                <IconButton
                                    aria-label="show more"
                                    aria-haspopup="true"
                                    color="inherit"
                                >
                                    <Login header={header} />
                                </IconButton>
                            </div>
                        }
                    </Toolbar>
                </AppBar>
                :

                'cargando'
            }

        </div>
    );
}

/*
import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Login from './Login';
import { db } from '../config/firestore';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    root: {
        width: '100%'
    }
}));

const Header = ({ handleDrawerOpen, open }) => {

    const classes = useStyles();

    const [header, setHeader] = useState({});
    useEffect(() => {
        const getMenuFirestore = () => {
            db.collection(`/portafolio/home/header`).get().then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    setHeader(doc.data())
                });
            });
        }
        getMenuFirestore();
    }, []);

    console.log(header)
    return (
        <AppBar
            position="fixed"
            className={clsx(classes.appBar, {
                [classes.appBarShift]: open,
            })}
        >
            <Toolbar>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={handleDrawerOpen}
                    edge="start"
                    className={clsx(classes.menuButton, {
                        [classes.hide]: open,
                    })}
                >
                    <MenuIcon />
                </IconButton>
                <div className={classes.root}>
                    <Grid
                        container
                        spacing={3}
                        direction="row"
                        justifyContent="space-between"
                        alignItems="center"
                    >
                        <Grid item xs={12} sm={2} md={2}>
                            <Typography variant="h6" noWrap>
                                Portafolio
                            </Typography>
                        </Grid>
                        <Grid item xs={12} sm={1} md={1}>
                            <Login icono={header.iconoLogin} />
                        </Grid>
                    </Grid>
                </div>
            </Toolbar>
        </AppBar>
    )
}

export default Header;
*/