import React, { useContext, useEffect, useState } from 'react';
import { useParams } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import ModuloContext from '../context/modulosContext/modulosContext';
import Personal from '../components/personal/SobreMi'
import Certificaciones from '../components/Certificaciones';
import Administrador from '../components/Administrador';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

export default function FullWidthGrid() {
    const classes = useStyles();
    let { opcion } = useParams();
    const { getMenu, menu } = useContext(ModuloContext)
    // const [compomente, setCompomente] = useState();

    useEffect(() => {
        getMenu();
    }, [])

    if (Object.keys(menu).length === 0) {
        return 'cargando'
    }

    let compomente = {};

    const renderizarMenuSelected = () => {
        // eslint-disable-next-line default-case
        switch (opcion) {
            case 'personal':
                compomente = <Personal />
                break;
            case 'certificaciones':
                compomente = <Certificaciones />
                break;
            case 'configuraciones':
                compomente = <Administrador />
                break;
            default:
                compomente = <Personal />
                break;
        }
    }

    renderizarMenuSelected();

    return (
        <div className={classes.root}>
            {compomente}
        </div>
    );
}