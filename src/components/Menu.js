import React, { useEffect, useState, useContext } from 'react';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import { makeStyles } from '@material-ui/core/styles';
import { Divider } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { Link, useRouteMatch } from "react-router-dom";
import app from '../config/firebase';
import { collection, getDocs, getFirestore } from "firebase/firestore";
import portafolioContext from '../context/authContext/portafolioContext';
import moduloContext from '../context/modulosContext/modulosContext';


const db = getFirestore(app);

const useStyles = makeStyles((theme) => ({
    avatar: {
        width: theme.spacing(5),
        height: theme.spacing(5),
    },
}));

const styles = {
    color: {
        solids: ['rgba(116, 72, 194, 1)', 'rgba(33, 192, 215, 1)', 'rgba(217, 158, 43, 1)', 'rgba(205, 58, 129, 1)', 'rgba(156, 153, 204, 1)', 'rgba(225, 78, 202, 1)'],
        alphas: ['rgba(116, 72, 194, .2)', 'rgba(33, 192, 215, .2)', 'rgba(217, 158, 43, .2)', 'rgba(205, 58, 129, .2)', 'rgba(156, 153, 204, .2)', 'rgba(225, 78, 202, .2)'],
    },
};

export default function Menu() {
    const classes = useStyles();
    const { lodged } = useContext(portafolioContext);
    const { menu, getMenu, menuAdmin, getMenuAdmin } = useContext(moduloContext);
    let { url } = useRouteMatch();

    useEffect(() => {
        getMenu();
        getMenuAdmin();
    }, []);

    return (
        <div style={{ background: '', height: '100%' }} >
            <List >
                {menu.map((m, index) => (
                    <Link to={`${url}/${m.nombre}`} style={{ textDecoration: 'none' }} key={m.nombre + index} >
                        <ListItem button key={m.nombre + index}>
                            <Avatar variant="square" alt="Remy Sharp" src={`${m.icono}`} className={classes.avatar} />
                            <Typography style={{ fontSize: 20, marginLeft: 15, color: styles.color.solids[4] }} variant="subtitle1" >
                                {m.nombre}
                            </Typography>
                        </ListItem>
                    </Link>
                ))}
            </List>
            <Divider />
            {lodged ?
                <List >
                    {menuAdmin.map((m, index) => (
                        <Link to={`${url}/${m.nombre}`} style={{ textDecoration: 'none' }} key={m.nombre + index}>
                            <ListItem button key={m.nombre + index}>
                                <Avatar variant="square" alt="Remy Sharp" src={`${m.icono}`} className={classes.avatar} />
                                <Typography style={{ fontSize: 20, marginLeft: 15, color: styles.color.solids[4] }} variant="subtitle1" color="textSecondary">
                                    {m.nombre}
                                </Typography>
                            </ListItem>
                        </Link>
                    ))}
                </List>
                :

                ''
            }
        </div>
    );
}
