import React, { useEffect, useState, useContext } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';
import portafolioContext from '../context/authContext/portafolioContext';

const useStyles = makeStyles((theme) => ({
    avatar: {
        width: theme.spacing(5),
        height: theme.spacing(5),
    },
}));

export default function FormDialog() {
    const classes = useStyles();
    const { configLogin, getConficLogin, getLogin, lodged } = useContext(portafolioContext);
    const [open, setOpen] = React.useState(false);
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [mostrarMensaje, setMostrarMensaje] = useState(false);
    const [mensaje, setMensaje] = useState('');

    useEffect(() => {
        getConficLogin();
    }, []);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const onchangeEmail = (e) => {
        setEmail(e.target.value)
    }

    const onchangePassword = (e) => {
        setPassword(e.target.value)
    }

    const cancelar = () => {
        setMostrarMensaje(false)
        handleClose()
        setEmail('')
        setPassword('')
    }

    const login = async () => {
        if (email.trim() === '' || password.trim() === '') {
            console.log('credenciales invalidas..!!')
            setMostrarMensaje(true);
            setMensaje(configLogin.mensajeError)
            return;
        }
        setMostrarMensaje(false);
        const data = await getLogin(email, password)
        if (!data) {
            setMostrarMensaje(true);
            setMensaje(configLogin.ErrorCredenciales)
            console.log('error en las credenciales ');
            return
        }
        cancelar();
        console.log('successfully');
    }

    return (
        <div>

            <Avatar
                alt="Remy Sharp"
                src={`${configLogin.icono}`}
                onClick={handleClickOpen}
                style={{ cursor: 'pointer' }}
                className={classes.avatar}

            />

            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">{configLogin.titulo}</DialogTitle>
                <DialogContent>
                    {mostrarMensaje ?
                        <DialogContentText>
                            {mensaje}
                        </DialogContentText>
                        :
                        ' '
                    }
                    <DialogContentText>
                        {configLogin.subTitulo}
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="email"
                        label={configLogin.inputEmail}
                        type="email"
                        onChange={onchangeEmail}
                        fullWidth
                    />
                    <TextField
                        margin="dense"
                        id="password"
                        label={configLogin.inputPassword}
                        type="password"
                        onChange={onchangePassword}
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => cancelar()} color="secondary">
                        Cancelar
                    </Button>
                    <Button onClick={() => login()} color="primary">
                        Ingresar
                    </Button>
                </DialogActions>
            </Dialog>

        </div>
    );
}
