import { useReducer } from 'react';
import PortafolioContext from './portafolioContext';
import PortafolioReducer from './portafolioReducer';
import app from "../../config/firebase";
import { collection, getDocs, getFirestore } from "firebase/firestore";
import {
    getAuth,
    signInWithEmailAndPassword,
    onAuthStateChanged,
} from "firebase/auth";

import {
    GET_CONFIG_LOGIN,
    GET_LOGIN,
    CERRAR_SESSION,
} from '../../types/index';

const auth = getAuth(app);
const db = getFirestore(app);

const PortafolioState = ({ children }) => {
    const initialState = {
        configLogin: [],
        lodged: localStorage.getItem('lodged') ? true : false,
        currenUser: {},
    };

    const [state, dispatch] = useReducer(PortafolioReducer, initialState);

    const setLocalStorage = (objeto) => {
        localStorage.setItem("lodged", JSON.stringify(objeto));
    }

    const getLocalStorage = () => {
        localStorage.getItem('lodged')
        return true;
    }

    const existeUsuarioAutenticado = () => {
        onAuthStateChanged(auth, (usuarioFirebase) => {
            if (usuarioFirebase) {
                //código en caso de que haya sesión inciiada
                const {
                    accessToken,
                    email,
                    emailVerified,
                    uid,
                } = usuarioFirebase.proactiveRefresh.user;

                const user = {
                    accessToken,
                    email,
                    emailVerified,
                    uid,
                }
                setLocalStorage(user)
            } else {
                //código en caso de que no haya sesión iniciada
                console.log('no esta autenticado');
            }
        });
    }

    const cerrarSesion = () => {
        auth.signOut();
        localStorage.clear();
        dispatch({
            type: CERRAR_SESSION,
        });
    }

    const getConficLogin = async () => {
        try {
            const querySnapshot = await getDocs(collection(db, `/portafolio/acceso/Login`));
            querySnapshot.forEach((doc) => {
                dispatch({
                    type: GET_CONFIG_LOGIN,
                    payload: doc.data(),
                });
            });
        } catch (error) {
            console.log(error);
        }
    };

    const getLogin = async (correo, password) => {
        let state;
        try {
            const restAuth = await signInWithEmailAndPassword(auth, correo, password);
            const {
                accessToken, email, emailVerified, uid,
            } = restAuth.user.proactiveRefresh.user;

            const user = {
                accessToken, email, emailVerified, uid,
            }
            setLocalStorage(user)
            dispatch({
                type: GET_LOGIN,
                payload: user,
            });
            state = true
        } catch (error) {
            console.log(error)
            state = false
        }
        return state;
    };

    return (
        <PortafolioContext.Provider
            value={{
                getConficLogin,
                getLogin,
                getLocalStorage,
                existeUsuarioAutenticado,
                cerrarSesion,
                lodged: state.lodged,
                configLogin: state.configLogin,
            }}
        >
            {children}
        </PortafolioContext.Provider>
    );
};

export default PortafolioState;
