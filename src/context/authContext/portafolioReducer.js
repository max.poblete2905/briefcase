/* eslint-disable import/no-anonymous-default-export */
import {
    GET_CONFIG_LOGIN,
    GET_LOGIN,
    GET_LOGIN_ERROR,
    CERRAR_SESSION,
} from '../../types/index';

export default (state, action) => {
    switch (action.type) {
        case GET_CONFIG_LOGIN:
            return {
                ...state,
                configLogin: action.payload,
            };
        case GET_LOGIN:
            return {
                ...state,
                lodged: true,
                currenUser: action.payload,
            };
        case GET_LOGIN_ERROR:
            return {
                ...state,
                lodged: false,
            };
        case CERRAR_SESSION:
            return {
                ...state,
                lodged: false,
                currenUser: {},
            };
        default:
            return state;
    }
};
