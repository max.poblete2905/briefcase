/* eslint-disable import/no-anonymous-default-export */
import {
    GET_MENU,
    GET_MENU_ADMIN,
} from '../../types/index';

export default (state, action) => {
    switch (action.type) {
        case GET_MENU:
            return {
                ...state,
                menu: action.payload,
            };
        case GET_MENU_ADMIN:
            return {
                ...state,
                menuAdmin: action.payload,
            };
        default:
            return state;
    }
};
