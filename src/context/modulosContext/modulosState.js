import { useReducer } from 'react';
import ModuloContext from './modulosContext';
import ModuloReducer from './moduloReducer';
import app from "../../config/firebase";
import { collection, getDocs, getFirestore } from "firebase/firestore";
import {
    GET_MENU,
    GET_MENU_ADMIN
} from '../../types/index';

const db = getFirestore(app);

const ModuloState = ({ children }) => {
    const initialState = {
        menu: [],
        menuAdmin: [],
    };

    const [state, dispatch] = useReducer(ModuloReducer, initialState);

    const getMenu = async () => {
        try {
            const querySnapshot = await getDocs(collection(db, `/portafolio/home/menu`));
            const listaMenu = [];
            querySnapshot.forEach((doc) => {
                listaMenu.push(doc.data())
            });
            dispatch({
                type: GET_MENU,
                payload: listaMenu,
            });
        } catch (error) {
            console.log(error);
        }
    };

    const getMenuAdmin = async () => {
        try {
            const listaMenuAdmin = [];
            const querySnapshot = await getDocs(collection(db, `/portafolio/home/menuAdmin`));
            querySnapshot.forEach((doc) => {
                listaMenuAdmin.push(doc.data());
            });
            dispatch({
                type: GET_MENU_ADMIN,
                payload: listaMenuAdmin,
            });
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <ModuloContext.Provider
            value={{
                getMenu,
                getMenuAdmin,
                menu: state.menu,
                menuAdmin: state.menuAdmin,
            }}
        >
            {children}
        </ModuloContext.Provider>
    );
};

export default ModuloState;
