import { useReducer } from 'react';
import PersonalContext from './personalContext';
import PersonalReducer from './personalReducer';
import app from "../../config/firebase";
import { collection, getDocs, getFirestore } from "firebase/firestore";

import {
    GET_CONFIG_PERSONAL,
    GET_CONFIG_FOTO,
    GET_CONFIG_OPCIONES,
} from '../../types/index';

const db = getFirestore(app);

const PersonalState = ({ children }) => {
    const initialState = {
        configPersonal: {},
        configFoto: {},
        configOpciones: {}
    };

    const [state, dispatch] = useReducer(PersonalReducer, initialState);

    const getConfigPersonal = async () => {
        try {
            let configModulo;
            const querySnapshot1 = await getDocs(collection(db, `/portafolio/modulos/configuraciones`));
            querySnapshot1.forEach((doc) => {
                if (doc.data().modulo === 'personal') {
                    configModulo = {
                        data: doc.data(),
                        id: doc.id,
                    }
                    dispatch({
                        type: GET_CONFIG_PERSONAL,
                        payload: configModulo,
                    });

                }

            });

            const querySnapshotFoto = await getDocs(collection(db, `/portafolio/modulos/configuraciones/${configModulo.id}/foto`));
            querySnapshotFoto.forEach((doc) => {
                dispatch({
                    type: GET_CONFIG_FOTO,
                    payload: doc.data(),
                });
            });

            const querySnapshotOpciones = await getDocs(collection(db, `/portafolio/modulos/configuraciones/${configModulo.id}/opciones`));
            const opcionesData = [];
            querySnapshotOpciones.forEach((doc) => {
                opcionesData.push(doc.data())
            });
            dispatch({
                type: GET_CONFIG_OPCIONES,
                payload: opcionesData,
            });
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <PersonalContext.Provider
            value={{
                getConfigPersonal,
                configPersonal: state.configPersonal,
                configFoto: state.configFoto,
                configOpciones: state.configOpciones,
            }}
        >
            {children}
        </PersonalContext.Provider>
    );
};

export default PersonalState;
