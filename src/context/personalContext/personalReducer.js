/* eslint-disable import/no-anonymous-default-export */
import {
    GET_CONFIG_PERSONAL,
    GET_CONFIG_FOTO,
    GET_CONFIG_OPCIONES,
} from '../../types/index';

export default (state, action) => {
    switch (action.type) {
        case GET_CONFIG_PERSONAL:
            return {
                ...state,
                configPersonal: action.payload,
            };
        case GET_CONFIG_FOTO:
            return {
                ...state,
                configFoto: action.payload,
            };
        case GET_CONFIG_OPCIONES:
            return {
                ...state,
                configOpciones: action.payload,
            };
        default:
            return state;
    }
};
