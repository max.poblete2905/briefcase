import { initializeApp } from "firebase/app";

const firebaseConfig = {
    apiKey: "AIzaSyAEF25rzQG3ytisnHvVhr1vBMKOmyt5cTk",
    authDomain: "portafolio-71b50.firebaseapp.com",
    projectId: "portafolio-71b50",
    storageBucket: "portafolio-71b50.appspot.com",
    messagingSenderId: "778473181505",
    appId: "1:778473181505:web:7f3f38c08d0faecd6c91c0"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export default app;

