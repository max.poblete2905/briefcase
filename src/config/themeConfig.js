/* istanbul ignore file */
import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({

  typography: {
    fontSize: 14,
    fontFamily: [
      'Ubuntu', 'sans-serif',
    ].join(','),
  },

  palette: {
    primary: {
      main: '#6A6A6A',
      light: '#EAE8E8',
    },
    secondary: {
      main: '#65C44A',
    },
    background: {
      paper: '#fff',
    },
  },
  shape: {
    borderRadius: 10,
  },

});
export default theme;
