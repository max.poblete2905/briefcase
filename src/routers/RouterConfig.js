import React from 'react';
import { Switch, Route, Redirect, useRouteMatch } from "react-router-dom";
import BaseSeccion from '../components/BaseSeccion'

const ConfigRouter = () => {

    let { url } = useRouteMatch();
    
    return (
        <Switch>
            <Route path={`${url}/:opcion`} component={BaseSeccion} />
            <Redirect to={`${url}`} />
        </Switch>
    );
}

export default ConfigRouter;